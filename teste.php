<?php 
/*
OBSERVAÇÕES DO TESTE
• Não estaremos verificando LAYOUT! e sim FUNCIONALIDADE! (Mas achamos importante pelo menos o uso do bootstrap.)
• Pode fazer tudo em um mesmo projeto.
• Não esqueça de colocar o bando de dados junto ao projeto.

=== TESTE DE LÓGICA === 
Crie uma classe Troco, que possui apenas um método, getQtdeNotas, que retorna a um array contendo a quantidade de notas necessárias de cada cédula, para completar o valor em reais passado no parâmetro.
Cas
*/

//EXEMPLO:

$troco = new Troco();
$notas = $troco->getQtdeNotas(289.99);
print_r($notas);

/*
Array
(
    [100] => 2
    [50] => 1
    [20] => 1
    [10] => 1
    [5] => 1
    [2] => 2
    [1] => 0
    [0.5] => 1
    [0.25] => 1
    [0.1] => 2
    [0.01] => 4
)
*/

/*

=== TESTE DE CONHECIMENTOS BASICOS CODEIGNITER === 
Crie um projeto simples com Codeigniter e HMVC.
• CRUD de Usuários (Para o login)
• CRUD de Clientes (Nome, CNPJ, Logo (Upload Simples))

Observações:
• Validar se CNPJ já existe na base de clientes.

!! DIFERENCIAIS !! 
• Utilize AngularJS (https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js) 
( Utilizamos e MUITO no nosso projeto. Formato: Header e Menu lateral fixos. Listagens e tabelas com Ajax utilizando angularjs também. )

• Pode utilizar utilizar Datamapper
• Exportar Clientes para Excel
• Impressão de Clientes

*/

?>